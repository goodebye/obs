defmodule Obs.Repo.Migrations.RemoveBalance do
  use Ecto.Migration

  def change do
    alter table(:accounts) do
      remove :balance
    end
  end
end
