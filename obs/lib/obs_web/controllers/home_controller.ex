defmodule ObsWeb.HomeController do
  use ObsWeb, :controller
  alias Obs.AccountContext, as: Account
  alias Obs.Stock

  plug ObsWeb.Plug.Authorize

  def index(conn, _params) do
    accounts = Account.get_accounts_for_user(conn.assigns[:user_id])

    token = get_session(conn, "token")

    tickers = %{
      "FB" => Stock.get_price("FB", token),
      "M" => Stock.get_price("M", token),
      "DIS" => Stock.get_price("DIS", token),
      "KHC" => Stock.get_price("KHC", token),
      "LYFT" => Stock.get_price("LYFT", token)
    }

    render(conn, "index.html", accounts: accounts, tickers: tickers)
  end
end
