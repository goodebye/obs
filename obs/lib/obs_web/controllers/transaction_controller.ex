defmodule ObsWeb.TransactionController do
  use ObsWeb, :controller
  alias Obs.Repo
  alias Obs
  alias Obs.TransactionContext, as: Transaction

  plug ObsWeb.Plug.Authorize

  def add_funds(conn, %{"amount" => amount, "account_id" => account_id}) do
    amount = Float.parse(amount) |> elem(0)

    case Transaction.add_funds(amount, account_id, conn.assigns[:user_id]) do
      {:error, reason} -> conn |> put_flash(:error, reason) |> redirect(to: "/")
      {:ok, _} -> conn |> put_flash(:info, "successfully added funds") |> redirect(to: "/")
    end
  end

  def create(conn, %{
        "tx" => tx_type,
        "ticker" => ticker,
        "account_id" => acct_id,
        "amount" => amount
      }) do
    amount = Integer.parse(amount) |> elem(0)

    case tx_type do
      "buy" ->
        case Transaction.buy(
               ticker,
               amount,
               acct_id,
               conn.assigns[:user_id],
               get_session(conn, "token")
             ) do
          {:ok, _} -> conn |> put_flash(:info, "successfully purchased") |> redirect(to: "/")
          {:error, reason} -> conn |> put_flash(:error, reason) |> redirect(to: "/")
        end

      "sell" ->
        case Transaction.sell(
               ticker,
               amount,
               acct_id,
               conn.assigns[:user_id],
               get_session(conn, "token")
             ) do
          {:ok, _} -> conn |> put_flash(:info, "successfully sold") |> redirect(to: "/")
          {:error, reason} -> conn |> put_flash(:error, reason) |> redirect(to: "/")
        end

      _ ->
        conn |> put_flash(:error, "unknown failure") |> redirect(to: "/")
    end
  end

  def create(conn, _) do
    conn |> put_flash(:error, "please fill out entire buy/sell form") |> redirect(to: "/")
  end
end
