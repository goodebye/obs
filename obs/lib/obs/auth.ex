defmodule Obs.Auth do
  @base_url Application.get_env(:obs, :auth_endpoint)

  def login(username, password) do
    login_path = @base_url <> "/login"

    case HTTPoison.post(
           login_path,
           Poison.encode!(%{"username" => username, "password" => password}),
           %{"Content-Type" => "application/json"}
         ) do
      {:ok, res} ->
        try do
          decoded_res =
            res
            |> Map.fetch(:body)
            |> elem(1)
            |> Poison.decode!()

          case decoded_res do
            %{"success" => true, "token" => token} -> {:ok, token}
            %{"success" => false, "reason" => reason} -> {:error, reason}
          end
        rescue
          _ -> {:error, "authorization service failure"}
        end

      {:error, _} ->
        {:error, "authorization service unavailable"}
    end
  end

  def register(username, password) do
    register_path = @base_url <> "/register"

    case HTTPoison.post(
           register_path,
           Poison.encode!(%{"username" => username, "password" => password}),
           %{"Content-Type" => "application/json"}
         ) do
      {:ok, res} ->
        try do
          decoded_res =
            res
            |> Map.fetch(:body)
            |> elem(1)
            |> Poison.decode!()

          case decoded_res do
            %{"success" => true, "token" => token} -> {:ok, token}
            %{"success" => false, "reason" => reason} -> {:error, reason}
          end
        rescue
          _ -> {:error, "authorization service failure"}
        end

      {:error, _} ->
        {:error, "authorization service unavailable"}
    end
  end
end
