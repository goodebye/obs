defmodule Obs.Repo do
  use Ecto.Repo,
    otp_app: :obs,
    adapter: Ecto.Adapters.Postgres
end
