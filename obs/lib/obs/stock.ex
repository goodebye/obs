defmodule Obs.Stock do
  @moduledoc """
  This is the module where all stock logic lives
  """

  @base_url Application.get_env(:obs, :facebook_endpoint)

  def get_price(ticker, token) do
    price_path = @base_url <> "/price/#{ticker}"

    case HTTPoison.get(
           price_path,
           %{"Content-Type" => "application/json", "token" => token}
         ) do
      {:ok, res} ->
        decoded_res =
          res
          |> Map.fetch(:body)
          |> elem(1)
          |> Poison.decode!()

        case decoded_res do
          %{"Response" => "Success", "ask" => ask} ->
            {:ok, ask}

          error ->
            IO.puts("okay\n\n\n")
            IO.inspect(error)
            {:error, "could not get price"}
        end

      {:error, _} ->
        {:error, "stock service not available"}
    end
  end

  def purchase_stock(ticker, amount, token) do
    buy_path = @base_url <> "/buy"

    case HTTPoison.post(
           buy_path,
           URI.encode_query(%{"shares" => "#{amount}", "token" => token, "symbol" => ticker}),
           %{"Content-Type" => "application/x-www-form-urlencoded"}
         ) do
      {:ok, res} ->
        decoded_res =
          res
          |> Map.fetch(:body)
          |> elem(1)
          |> Poison.decode!()

        case decoded_res do
          %{"user_id" => user_id, "shares" => shares, "quoted_price" => price} ->
            {:ok, %{shares: shares, price: price, user_id: user_id}}

          %{"Response" => response} ->
            {:error, response}
        end

      {:error, _} ->
        {:error, "stock service not available"}
    end
  end

  def sell_stock(ticker, amount, token) do
    buy_path = @base_url <> "/sell"

    case HTTPoison.post(
           buy_path,
           URI.encode_query(%{"shares" => "#{amount}", "token" => token, "symbol" => ticker}),
           %{"Content-Type" => "application/x-www-form-urlencoded"}
         ) do
      {:ok, res} ->
        decoded_res =
          res
          |> Map.fetch(:body)
          |> elem(1)
          |> Poison.decode!()

        case decoded_res do
          %{"user_id" => user_id, "shares" => shares, "quoted_price" => price} ->
            {:ok, %{shares: shares, price: price, user_id: user_id}}

          %{"Response" => response} ->
            {:error, response}
        end

      {:error, _} ->
        {:error, "stock service not available"}
    end
  end
end
