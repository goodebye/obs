defmodule ObsWeb.ErrorViewTest do
  use ObsWeb.ConnCase, async: true

  # Bring render/3 and render_to_string/3 for testing custom views
  import Phoenix.View

  # Unit
  test "renders 404.html" do
    assert render_to_string(ObsWeb.ErrorView, "404.html", []) == "Not Found"
  end

  # Unit
  test "renders 500.html" do
    assert render_to_string(ObsWeb.ErrorView, "500.html", []) == "Internal Server Error"
  end
end
