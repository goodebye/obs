# OBS Project
Chandler Griffin, Jonas Pena, Cameron Rodgers, Olive Vaughn, Erin Yoo

## Production build

[**Dashboard**](https://sonic-column-261820.appspot.com/)

[**Auth service**](https://auth-dot-sonic-column-261820.appspot.com/)

[**Stock service**](https://facebook-261621.appspot.com/)

## API docs

[**Swagger API Documentation**](https://app.swaggerhub.com/apis/griffincc/OBS_StockExchange/1.0.0)

## Video
Our video can be viewed here:
https://drive.google.com/file/d/1obQUhbDVoP4z-mDZpZFouG32bxlcfum5/view

## General Requirements
For all microservices or general OBS project you will need these dependencies and installations:
 - [**Elixir**](https://elixir-lang.org/install.html)
 - [**PostgreSQL**](https://www.postgresql.org/docs/9.3/tutorial-install.html)
 
## How To Use
### Development build
Simply `cd` into the `/auth` and `/stock_exchanges/facebook` directories and run `mix deps.get && mix ecto.create && mix ecto.migrate` followed by `iex -S mix` in each. Then cd into /obs and run `mix deps.get && mix ecto.create && mix ecto.migrate` followed by `mix phx.server`. 

After all applications are running, you can find them at localhost at the ports 4005, 4006 and 4001 (auth, dashboard, and stock exchange services respectively.)

### Testing
`cd` into `/auth` or `/stock_exchanges/[ticker]` or `/obs` and run `mix deps.get` and then `mix ecto.create && mix ecto.migrate && mix test` for the first run. On subsequent runs, execute `mix ecto.drop && mix ecto.create && mix ecto.migrate && mix test`.

## Setup Summary
In order to complete the project we created 3 main applications using the Elixir programming language. These were the OBS main application, the auth microservice, and the stock exchange microservice. The stock exchanges and auth microservices utilized plain Elixir with some HTTP libraries (Plug, Ecto), while the OBS application used the Elixir Phoenix Framework. We chose the Phoenix framework because it provides high productivity and high application performance. We used JWT tokens in order to authenticate users and make sure they were signed in before performing any transaction involving the stock exchanges. The CI service used by our team was Gitlab CI. We chose to use Gitlab CI due to the fact that we were already using Gitlab for as our repository, and the hosted solution was more than enough for our purposes.

To persist data, such as user account information, we decided to use PostgreSQL. There are two main reasons why we decided to use PostgreSQL. The first reason is that there is an abundance of documentation online for working with Elixir and Postgres. The next reason is that Elixir has a nifty database wrapper and query generator, called Ecto, that works with PostgreSQL. In our project we use a database for the stock exchanges, one for user account information, and one that stores the OBS user accounts. We used Google App Engine for our deployments, and CloudSQL for our PostgreSQL host. Our configuration management included .gitlab-ci.yml for the CI configuration (defining pipelines, commands, and the Docker image and services for our test runners), App Engine user management for generating accounts for use during the CI/CD pipeline, and App Engine app.yml files for configuring the deploy/build image and compilation.

# Known issues
On the Dashboard, an expired browser session causes infinite redirects. To solve, go to https://sonic-column-261820.appspot.com/logout to logout, then you can log back in as normal.

## Milestone #3 Information
Milestone information can be found at either of these points:
 - **Branch:** [master]
 - **Commit #:** milestone3 [9b3f47bc]

## Test Coverage Report
 - **Disney** ![](./assets/milestone1/disney.png)
 - **Facebook** !![](./assets/milestone1/facebook.png)
 - **Kraft Heinz** ![](./assets/milestone1/kraftheinz.png)
 - **Lyft** ![](./assets/milestone1/lyft.png)
 - **Macys** ![](./assets/milestone1/macys.png)
