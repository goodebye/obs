use Mix.Config

config :macys, Repo,
  database: "macys",
  username: System.get_env("POSTGRES_USER") || "postgres",
  password: System.get_env("POSTGRES_USER") || "postgres",
  hostname: System.get_env("POSTGRES_HOST") || "localhost"

config :macys, Macys, API_TOKEN: System.get_env("API_TOKEN")

config :macys, ecto_repos: [Repo]

config :joken, default_signer: "secret"

config :logger, level: :info

if Mix.env() == :test do
  import_config "#{Mix.env()}.exs"
end
