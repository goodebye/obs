defmodule Facebook do
  use Application

  def start(_type, _args) do
    potential_port = System.get_env("PORT")

    port =
      cond do
        potential_port != nil and is_binary(potential_port) ->
          potential_port |> String.to_integer()

        potential_port != nil and is_integer(potential_port) ->
          potential_port

        true ->
          4001
      end

    # List all child processes to be supervised
    children = [
      # Start the Ecto repositor
      Repo,
      Plug.Adapters.Cowboy.child_spec(
        scheme: :http,
        plug: Router,
        options: [port: port]
      )
    ]

    opts = [strategy: :one_for_one]

    {:ok, pid} = Supervisor.start_link(children, opts)
    symbols = ["FB", "DIS ", "KHC", "LYFT", "M"]

    Enum.each(symbols, fn symbol ->
      init_shares(symbol)
    end)

    {:ok, pid}
  end

  def init_shares(symbol) do
    IO.puts(symbol)

    case Functions.get_bank_shares(symbol) do
      0 ->
        header = [
          Authorization: "Bearer #{System.get_env("API_TOKEN")}",
          Accept: "application/json"
        ]

        {:ok, %HTTPoison.Response{body: body}} =
          HTTPoison.get("https://sandbox.tradier.com/v1/markets/quotes?symbols=#{symbol}", header)

        IO.inspect(System.get_env("API_TOKEN"))
        decoded_body = Jason.decode!(body)

        %{
          "quotes" => %{
            "quote" => %{
              "last" => last
            }
          }
        } = decoded_body

        total_cost = 5000 * last

        Repo.insert(%Ledger{
          type: "buy",
          shares: 5000,
          quoted_price: last,
          total_cost: total_cost,
          symbol: symbol
        })

        Repo.insert(%Logs{
          type: "buy",
          user_id: 0,
          shares: 5000,
          quoted_price: last,
          symbol: symbol
        })

        {:ok}

      _ ->
        nil
    end
  end
end
