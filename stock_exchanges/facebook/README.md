# Facebook
Stock microservice for Facebook. Below are instructions to spin up the microservice by itself and run tests.

See [Swagger API Documentation](https://app.swaggerhub.com/apis/griffincc/OBS_StockExchange/1.0.0) for information on endpoints.

## Setup
Things that need to be installed before running microservice:
 - [Elixir](https://elixir-lang.org/install.html)
 - [PostgreSQL](https://www.postgresql.org/docs/9.3/tutorial-install.html)
 - Sign up for an account at [Tradier](https://developer.tradier.com/) to obtain an **API Key**

## Run Service
Follow the steps to get dependencies and run the service:
 1. At the root of the microservice folder run `mix deps.get`
 2. After dependencies are installed run `mix ecto.create` (creates database)
 3. Migrate our relations using the command `mix ecto.migrate`
 4. Then to run the service run the command `iex -S mix run`
    - This will run the service in an interactive mode, learn more about commands [here](https://hexdocs.pm/iex/IEx.html)
    - The service is also running on localhost:4000
 5. When the service is up and running, you will need a user token. Generate one in the interactive mode:
    - `{:ok, token, _claims} = Token.generate_and_sign(%{"user_id" => 1})`
    - Now your generated token can be used by calling the variable token

## Run Tests
Follow the steps to get tests for the microservice to run:
 1. Ensure that all dependencies are installed with `mix deps.get`
 2. Ensure database is created with `mix ecto.create`
 3. Ensure you've migrated all relations with `mix ecto.migrate`
 4. Then at the root of the microservice folder, run: `API_TOKEN="{yourkeyhere}" mix test`
    - Ensure that you've replaced `{yourkeyhere}` with the **API key** you've retrieved from Tradier.

If any issues occur during the test where the database or relational table cannot be found run:
 - `mix ecto.rollback`
 - then run `mix ecto.migrate`
 - then run the command for the tests again

