use Mix.Config

config :disney, Repo,
  database: "disney",
  username: System.get_env("POSTGRES_USER") || "postgres",
  password: System.get_env("POSTGRES_USER") || "postgres",
  hostname: System.get_env("POSTGRES_HOST") || "localhost"

config :disney, Disney, API_TOKEN: System.get_env("API_TOKEN")

config :disney, ecto_repos: [Repo]

config :joken, default_signer: "secret"

config :logger, level: :info

if Mix.env() == :test do
  import_config "#{Mix.env()}.exs"
end
