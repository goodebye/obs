defmodule Repo do
  use Ecto.Repo,
    otp_app: :disney,
    adapter: Ecto.Adapters.Postgres
end
