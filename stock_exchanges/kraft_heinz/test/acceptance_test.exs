Ecto.Adapters.SQL.Sandbox.mode(Repo, :manual)

defmodule AcceptanceTest do
  use ExUnit.Case, async: true
  use Plug.Test

  @opts Router.init([])

  setup do
    # Explicitly get a connection before each test
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(Repo)
  end

  test "successfully connects to Tradier API" do
    {:ok, token, _claims} = Token.generate_and_sign(%{"user_id" => 1})

    conn =
      conn(:get, "/price/KHC/", %{})
      |> put_req_header("token", token)

    conn = Router.call(conn, @opts)
    assert conn.status == 200
  end

  test "lists all transactions for single admin" do
    {:ok, token, _claims} = Token.generate_and_sign(%{"user_id" => 0})

    conn =
      conn(:get, "/logs", %{})
      |> put_req_header("token", token)

    conn = Router.call(conn, @opts)
    assert conn.status == 200
  end

  test "allows OBS to buy stock at current price" do
    {:ok, token, _claims} = Token.generate_and_sign(%{"user_id" => 3})
    conn = conn(:post, "/buy", %{"shares" => "0", "token" => token})

    conn = Router.call(conn, @opts)
    assert conn.status == 200
  end

  test "allows OBS to sell stock at current price" do
    {:ok, token, _claims} = Token.generate_and_sign(%{"user_id" => 3})
    conn = conn(:post, "/sell", %{"shares" => "0", "token" => token})

    conn = Router.call(conn, @opts)
    assert conn.status == 200
  end

  test "allows OBS to see profit/loss" do
    {:ok, token, _claims} = Token.generate_and_sign(%{"user_id" => 0})

    conn =
      conn(:get, "/pl", %{})
      |> put_req_header("token", token)

    conn = Router.call(conn, @opts)
    assert conn.status == 200
  end

  test "allows OBS to see users current holdings" do
    {:ok, token, _claims} = Token.generate_and_sign(%{"user_id" => 4})

    conn =
      conn(:get, "/holdings", %{})
      |> put_req_header("token", token)

    conn = Router.call(conn, @opts)
    assert conn.status == 200
  end

  test "invalid token fails to get profit loss" do
    conn =
      conn(:get, "/pl", %{})
      |> put_req_header("token", "wrongToken")

    conn = Router.call(conn, @opts)
    assert conn.status == 404
  end

  test "invalid token fails to get user holdings" do
    conn =
      conn(:get, "/holdings", %{})
      |> put_req_header("token", "wrongToken")

    conn = Router.call(conn, @opts)
    assert conn.status == 404
  end

  test "invalid token does not allow OBS to sell" do
    conn = conn(:post, "/sell", %{"shares" => "0", "token" => "wrongToken"})

    conn = Router.call(conn, @opts)
    assert conn.status == 404
  end

  test "invalid token does not allow OBS to buy" do
    conn = conn(:post, "/buy", %{"shares" => "0", "token" => "wrongToken"})

    conn = Router.call(conn, @opts)
    assert conn.status == 404
  end

  test "fails to list transaction for invalid admin" do
    conn =
      conn(:get, "/logs", %{})
      |> put_req_header("token", "wrongToken")

    conn = Router.call(conn, @opts)
    assert conn.status == 404
  end

  describe "Database Acceptance Tests" do
    test "stores all transactions in database" do
      {:ok, token, _claims} = Token.generate_and_sign(%{"user_id" => 5})
      conn = conn(:post, "/buy", %{"shares" => "0", "token" => token})

      conn = Router.call(conn, @opts)
      assert conn.status == 200

      {:ok, token, _claims} = Token.generate_and_sign(%{"user_id" => 5})
      conn = conn(:post, "/sell", %{"shares" => "0", "token" => token})

      conn = Router.call(conn, @opts)
      assert conn.status == 200

      assert length(Repo.all(Ledger)) == 3
    end
  end

  describe "Inventory Acceptance Tests" do
    test "starts stock inventory with 5000 shares" do
      assert Functions.get_bank_shares() == 5000
    end

    test "does not allow inventory to be zero" do
      # buy 5000 shares and then test that shares is still 5000
      {:ok, token, _claims} = Token.generate_and_sign(%{"user_id" => 4})
      conn = conn(:post, "/buy", %{"shares" => "5000", "token" => token})

      conn = Router.call(conn, @opts)
      assert conn.status == 200
      assert Functions.get_bank_shares() == 5000
    end
  end
end
