defmodule Ledger do
  use Ecto.Schema

  schema "ledger" do
    field(:type, :string)
    field(:shares, :integer)
    field(:quoted_price, :float)
    field(:total_cost, :float)

    timestamps()
  end
end
