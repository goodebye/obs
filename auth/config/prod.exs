use Mix.Config

config :auth, Auth.Repo,
  database: "auth_repo",
  username: System.get_env("POSTGRES_USER") || "postgres",
  password: System.get_env("POSTGRES_PASSWORD") || "password",
  socket_dir:
    System.get_env("SOCKET_DIR") || "/tmp/cloudsql/sonic-column-261820:us-east1:obsdb-prod",
  pool_size: 15
