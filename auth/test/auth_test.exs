defmodule AuthTest do
  use ExUnit.Case
  alias Auth.Controller

  # Integration
  test "validates username/password stored in database" do
    # first register a user
    Controller.register("test_user", "password")

    result = Controller.login("test_user", "password")

    assert {:ok, _} = result
  end

  # Integration
  test "auth fails on non-existent user" do
    assert {:error, _} = Controller.login("fakeuser", "fakepass")
  end

  # Integration
  test "returns auth token on successful login" do
    Controller.register("test_user_2", "password")

    {:ok, token} = Controller.login("test_user_2", "password")

    # confirm our token holds the user_id claim for use on other services
    {:ok, claims} = Auth.Token.verify_and_validate(token)

    assert %{"user_id" => user_id} = claims
    assert is_integer(user_id)
  end
end
