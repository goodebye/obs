defmodule Auth.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    potential_port = System.get_env("PORT")

    port =
      cond do
        potential_port != nil and is_binary(potential_port) ->
          potential_port |> String.to_integer()

        potential_port != nil and is_integer(potential_port) ->
          potential_port

        true ->
          4005
      end

    children = [
      # Starts a worker by calling: Auth.Worker.start_link(arg)
      # {Auth.Worker, arg}
      {Auth.Repo, []},
      Plug.Cowboy.child_spec(
        scheme: :http,
        plug: Auth.Endpoint,
        options: [port: port]
      )
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Auth.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
