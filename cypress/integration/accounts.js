let username = "usernameTest"
let password = "longerthan8ok"

// **Scenario**: Bob wants to add funds to his account to buy more stocks
//   - **Given**: Bob already has an OBS account registered AND is signed in
//   - **When**: Bob opens the add funds page AND enters a valid number
//   - **Then**: Bob is redirected to the dashboard AND sees a confirmation that funds have been added AND the OBS dashboard should be updated with the correct amount
describe("User already has an OBS account registered AND is signed in", function()    {
    it("User opens the add funds page AND enters a valid number", function()    {
        cy.launchOBS()
        cy.register("acctest1", password)

        
        cy.createAccount("blue")
        cy.get('form[action="/transactions/add_funds"]>input').eq(0).type("500")
        cy.get('form[action="/transactions/add_funds"]>input').eq(2).click()
        cy.contains("successfully added funds")
        cy.get('tbody>tr>td').eq(1).should('contain', "500.0")
    })
})

// **Scenario**: Bob wants to create another account to buy different stocks
//   - **Given**: Bob already has an OBS account registered
//   - **When**: Bob opens the Registration page in a web browser AND properly enters the required fields
//   - **Then**: Bob sees a registration confirmation AND is logged in to the system AND is redirected to the dashboard AND database is updated with another account for the same user_id
describe("User already has an OBS account registered", function()    {
    it("User clicks to add a new account AND properly enters the required fields", function()    {
        cy.launchOBS()
        cy.register("accttest2", password)

        cy.createAccount("blue")

        cy.contains("account created")
    })
})

// // *Scenario**: Logged in user Bob wants to open more than 3 accounts
// //   - **Given**: Bob is an authorized user AND has 3 existing accounts
// //   - **When**: Bob opens up the Create Account page in a web browser AND submits the form to open an additional account
// //   - **Then**: Bob sees an error message on the Create Account page that indicates he has opened the maximum number of accounts
// describe("User is an authorized user AND has 3 existing accounts", function()    {
//     it("User opens up the Create Account page in a web browser AND submits the form to open an additional account", function()    {
//         cy.launchOBS()
//         cy.register("acctest3", password)

//         cy.createAccount("blue")
//         cy.createAccount("red")
//         cy.createAccount("green")
//         cy.createAccount("yellow")

//         cy.contains("cannot create account")
//     })
// })
